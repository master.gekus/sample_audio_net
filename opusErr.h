#ifndef OPUS_ERR_H
#define OPUS_ERR_H

#include <QString>

QString getOpusError(int errorCode);

#endif // OPUS_ERR_H
