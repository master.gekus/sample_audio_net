#ifndef IAS_H
#define IAS_H

#include <QObject>

class ias : public QObject {
  Q_OBJECT

 public:
  enum : size_t {
    kSampleRate = 48000,
    kChannels = 1,
    kBytesPerSample = 2,
    kFrameSizeInSamples = kSampleRate / 50,
    kFrameSizeInBytes = kFrameSizeInSamples * kChannels * kBytesPerSample,
  };

 public:
  explicit ias(QObject *parent = nullptr);
  ~ias() override;

 signals:
};

#endif  // IAS_H
