############################################################################
#
# - Find the opus include file and library
#
#  OPUS_FOUND - system has opus
#  OPUS_INCLUDE_DIRS - the opus include directory
#  OPUS_LIBRARIES - The libraries needed to use opus

if (OPUS_INCLUDE_DIRS AND OPUS_LIBRARIES)
  # in cache already
  set(OPUS_PORTAUDIO_FOUND TRUE)
else (OPUS_INCLUDE_DIRS AND OPUS_LIBRARIES)

    find_path(OPUS_INCLUDE_DIRS
      NAMES opus/opus.h
      PATH_SUFFIXES include
  )
  if(OPUS_INCLUDE_DIRS)
      set(HAVE_OPUS_OPUS_H 1)
  endif()

  find_library(OPUS_LIBRARIES NAMES opus)

  if(OPUS_LIBRARIES)
      find_library(LIBM NAMES m)
      if(LIBM)
    list(APPEND OPUS_LIBRARIES ${LIBM})
      endif()
  endif()

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(Opus
      DEFAULT_MSG
      OPUS_INCLUDE_DIRS OPUS_LIBRARIES HAVE_OPUS_OPUS_H
  )

  mark_as_advanced(OPUS_INCLUDE_DIRS OPUS_LIBRARIES HAVE_OPUS_OPUS_H)

endif(OPUS_INCLUDE_DIRS AND OPUS_LIBRARIES)

add_library(opus INTERFACE)
target_link_libraries(opus INTERFACE ${OPUS_LIBRARIES})
target_include_directories(opus INTERFACE ${OPUS_INCLUDE_DIRS})
add_library(Opus::Opus ALIAS opus)
