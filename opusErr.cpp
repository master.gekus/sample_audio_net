#include "opusErr.h"

#include <opus/opus_defines.h>

QString getOpusError(int errorCode) {
  switch (errorCode) {
    case OPUS_OK:
      return QStringLiteral("No error.");

    case OPUS_BAD_ARG:
      return QStringLiteral("One or more invalid/out of range arguments.");

    case OPUS_BUFFER_TOO_SMALL:
      return QStringLiteral("Not enough bytes allocated in the buffer.");

    case OPUS_INTERNAL_ERROR:
      return QStringLiteral("An internal error was detected.");

    case OPUS_INVALID_PACKET:
      return QStringLiteral("The compressed data passed is corrupted.");

    case OPUS_UNIMPLEMENTED:
      return QStringLiteral("Invalid/unsupported request number");

    case OPUS_INVALID_STATE:
      return QStringLiteral(
          "An encoder or decoder structure is invalid or already freed.");

    case OPUS_ALLOC_FAIL:
      return QStringLiteral("Memory allocation has failed.");

    default:
      return QStringLiteral("<undefined error #%1>").arg(errorCode);
  }
}
